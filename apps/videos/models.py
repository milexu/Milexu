from django.db import models
from django.contrib.auth.models import User

class Video(models.Model):
    video_name = models.CharField(max_length=100)
    video_upload = models.FileField(upload_to="videos/")
    video_desc = models.CharField(max_length=10000)
    video_author = models.ForeignKey(User, on_delete=models.CASCADE)
    video_date = models.DateTimeField(auto_now_add=True)

class VideoUpvote(models.Model):
    video_upvote = models.ForeignKey(Video, on_delete=models.CASCADE)
    video_upvote_author = models.ForeignKey(User, on_delete=models.CASCADE)
    video_upvote_date = models.DateTimeField(auto_now_add=True)

class VideoDownvote(models.Model):
    video_downvote = models.ForeignKey(Video, on_delete=models.CASCADE)
    video_downvote_author = models.ForeignKey(User, on_delete=models.CASCADE)
    video_downvote_date = models.DateTimeField(auto_now_add=True)