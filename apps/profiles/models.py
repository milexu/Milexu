from django.db import models
from django.contrib.auth.models import User

class Profiles(models.Model):
  display_name = models.CharField(max_length=30)
  avatar = models.ImageField(upload_to='avatars/')
  banner = models.ImageField(upload_to='banners/')
  bio = models.CharField(max_length=2000)